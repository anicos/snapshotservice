package pl.anicos.snapshot.model;

/**
 * Created by anicos on 12/18/15.
 */
public enum ResultStatus {
    SUCCESS, FAILURE
}
