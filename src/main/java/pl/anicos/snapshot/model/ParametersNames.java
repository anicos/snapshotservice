package pl.anicos.snapshot.model;

public final class ParametersNames {

    public static final String THUMBNAIL_WIDTH = "thumbnailWidth";
    public static final String THUMBNAIL_HEIGHT = "thumbnailHeight";
    public static final String URL = "url";
    public static final String WINDOW_WIDTH = "windowWidth";
    public static final String WINDOW_HEIGHT = "windowHeight";
    public static final String TIME_FOR_RENDERING_PAGE = "timeForRenderingPage";

    private ParametersNames() {
    }
}
