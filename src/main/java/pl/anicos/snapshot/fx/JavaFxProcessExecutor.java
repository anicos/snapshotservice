package pl.anicos.snapshot.fx;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import pl.anicos.snapshot.exception.PageNotFoundException;
import pl.anicos.snapshot.model.ResultStatus;

import java.io.*;
import java.util.concurrent.TimeUnit;

@Component
public class JavaFxProcessExecutor {

    private final Log log = LogFactory.getLog(getClass());

    public String exec(Class clazz, String args) throws IOException,
            InterruptedException {
        ProcessBuilder builder = createProcessBuilder(clazz, args);

        Process process = builder.start();
        BufferedReader reader = getBufferedReader(process);

        String line;

        while ((line = reader.readLine()) != null) {

            if (line.startsWith(ResultStatus.SUCCESS.name())) {
                closeProcess(process, reader);
                log.info("page opened with success");
                return line.substring(ResultStatus.SUCCESS.name().length());
            }

            if (line.startsWith(ResultStatus.FAILURE.name())) {
                closeProcess(process, reader);
                log.info("page opened with failure. Args for javaFX: "+args);
                throw new PageNotFoundException();
            }
        }

        if (!process.waitFor(30, TimeUnit.SECONDS)) {
            log.info("process timeout " + "Args for javaFX: "+args);
            closeProcess(process, reader);
            throw new PageNotFoundException();
        }
        log.info("process without result " + "Args for javaFX: "+args);

        throw new PageNotFoundException();
    }

    private void closeProcess(Process process, BufferedReader reader) throws IOException {
        reader.close();
        process.destroy();
    }

    private BufferedReader getBufferedReader(Process process) {
        InputStream stdout = process.getInputStream();
        return new BufferedReader(new InputStreamReader(stdout));
    }

    private ProcessBuilder createProcessBuilder(Class klass, String args) {
        String javaHome = System.getProperty("java.home");
        String javaBin = javaHome +
                File.separator + "bin" +
                File.separator + "java";
        String classpath = System.getProperty("java.class.path");
        String className = klass.getCanonicalName();

        ProcessBuilder builder = new ProcessBuilder(
                javaBin, "-cp", classpath, className, args);

        builder.redirectErrorStream(true);
        return builder;
    }
}